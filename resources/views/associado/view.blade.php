@extends('template.admin')

@section('css')
@endsection

@section('corpo')
<div class="d-flex flex-column flex-column-fluid">
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-xxl">
            <div class="card mb-5 mb-xl-10">
                <div class="card-body pt-9 pb-0">
                    <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                        <div class="me-7 mb-4">
                            <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                                <img src="/assets/media/avatars/300-1.jpg" alt="image" />
                                <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-body h-20px w-20px"></div>
                            </div>
                        </div>
                        <div class="flex-grow-1">
                            <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                                <div class="d-flex flex-column">
                                    <div class="d-flex align-items-center mb-2">
                                        <a href="#" class="text-gray-900 text-hover-primary fs-2 fw-bold me-1">{{$associado->nome}}</a>
                                        <a href="#">
                                            <span class="svg-icon svg-icon-1 svg-icon-primary">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                                    <path d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z" fill="currentColor" />
                                                    <path d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z" fill="white" />
                                                </svg>
                                            </span>
                                        </a>
                                        @switch($associado->situacao)
                                            @case('ATIVO')
                                            <a href="#" class="btn btn-sm btn-light-success fw-bold ms-2 fs-8 py-1 px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">ATIVO</a>
                                                @break
                                            @case('PENDENTE')
                                            <a href="#" class="btn btn-sm btn-light-warning fw-bold ms-2 fs-8 py-1 px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">PENDENTE</a>
                                                @break
                                            @case('NEGADO')
                                            <a href="#" class="btn btn-sm btn-light-dark fw-bold ms-2 fs-8 py-1 px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">NEGADO</a>
                                                @break
                                            @case('INATIVO/PAGAMENTO')
                                            <a href="#" class="btn btn-sm btn-light-danger fw-bold ms-2 fs-8 py-1 px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">INATIVO/PAGAMENTO</a>
                                                @break
                                            @case('INATIVO')
                                            <a href="#" class="btn btn-sm btn-light-danger fw-bold ms-2 fs-8 py-1 px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">INATIVO</a>
                                            @break
                                            @case('INADIMPLENTE')
                                            <a href="#" class="btn btn-sm btn-light-info fw-bold ms-2 fs-8 py-1 px-3" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">INADIMPLENTE</a>
                                            @break

                                            @default

                                        @endswitch
                                    </div>
                                    <div class="d-flex flex-wrap fw-semibold fs-6 mb-4 pe-2">
                                        @php
                                            $celular = substr_replace($associado->celular, '(', 0, 0);
                                            $celular = substr_replace($celular, '9', 3, 0);
                                            $celular = substr_replace($celular, ')', 3, 0);
                                            $celular = substr_replace($celular, '-', 9, 0);
                                        @endphp
                                        <a href="https://api.whatsapp.com/send?phone=55{{$associado->celular}}" target="_blank" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                        <span class="svg-icon svg-icon-4 me-1">
                                            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path opacity="0.3" d="M16.5 9C16.5 13.125 13.125 16.5 9 16.5C4.875 16.5 1.5 13.125 1.5 9C1.5 4.875 4.875 1.5 9 1.5C13.125 1.5 16.5 4.875 16.5 9Z" fill="currentColor" />
                                                <path d="M9 16.5C10.95 16.5 12.75 15.75 14.025 14.55C13.425 12.675 11.4 11.25 9 11.25C6.6 11.25 4.57499 12.675 3.97499 14.55C5.24999 15.75 7.05 16.5 9 16.5Z" fill="currentColor" />
                                                <rect x="7" y="6" width="4" height="4" rx="2" fill="currentColor" />
                                            </svg>
                                        </span>

                                        {{$celular}}</a>
                                        <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                        <span class="svg-icon svg-icon-4 me-1">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path opacity="0.3" d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z" fill="currentColor" />
                                                <path d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z" fill="currentColor" />
                                            </svg>
                                        </span>
                                        {{$associado->cidade}}</a>
                                        <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary mb-2">
                                        <span class="svg-icon svg-icon-4 me-1">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path opacity="0.3" d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19Z" fill="currentColor" />
                                                <path d="M21 5H2.99999C2.69999 5 2.49999 5.10005 2.29999 5.30005L11.2 13.3C11.7 13.7 12.4 13.7 12.8 13.3L21.7 5.30005C21.5 5.10005 21.3 5 21 5Z" fill="currentColor" />
                                            </svg>
                                        </span>
                                        {{$associado->email}}</a>
                                    </div>
                                </div>
                                <div class="d-flex my-4">
                                    <a href="#" class="btn btn-sm btn-primary me-2" data-bs-toggle="modal" data-bs-target="#kt_modal_offer_a_deal">Editar</a>

                                </div>
                            </div>
                            <div class="d-flex flex-wrap flex-stack">
                                <div class="d-flex flex-column flex-grow-1 pe-8">
                                    <div class="d-flex flex-wrap">
                                        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                            <div class="d-flex align-items-center">
                                                <div class="fs-2 fw-bold" data-kt-countup="true" data-kt-countup-value="369" data-kt-countup-prefix="R$">0</div>
                                            </div>
                                            <div class="fw-semibold fs-6 text-gray-400">3 boletos</div>
                                        </div>
                                        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                            <div class="d-flex align-items-center">
                                                <div class="fs-2 fw-bold" data-kt-countup="true" data-kt-countup-value="3">0</div>
                                            </div>
                                            <div class="fw-semibold fs-6 text-gray-400">Veículos</div>
                                        </div>
                                        <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                            <div class="d-flex align-items-center">
                                                <div class="fs-2 fw-bold" data-kt-countup="true" data-kt-countup-value="6">0</div>
                                            </div>
                                            <div class="fw-semibold fs-6 text-gray-400">Eventos <small>ultimos 12 meses</small></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
                                    <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                        <span class="fw-semibold fs-6 text-gray-400">Contrato</span>
                                        <span class="fw-bold fs-6">50%</span>
                                    </div>
                                    <div class="h-5px mx-3 w-100 bg-light mb-3">
                                        <div class="bg-success rounded h-5px" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold">
                        <li class="nav-item mt-2">
                            <a class="nav-link text-active-primary ms-0 me-10 py-5 active" href="#">Dados do Associado</a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link text-active-primary ms-0 me-10 py-5" href="#">Veículos</a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link text-active-primary ms-0 me-10 py-5" href="#">Financeiro</a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link text-active-primary ms-0 me-10 py-5" href="#">Eventos</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                <div class="card-body p-9">
                    <div class="row mb-7">
                        <label class="col-lg-2 fw-semibold text-muted">Nome Completo</label>
                        <div class="col-lg-4">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->nome}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">CPF/CNPJ</label>
                        <div class="col-lg-4">
                            @php
                                $cnpj_cpf = preg_replace("/\D/", '', $associado->cpfcnpj);
                                if (strlen($cnpj_cpf) === 11) {
                                    $cnpj_cpf = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
                                }else{
                                    $cnpj_cpf = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                }
                            @endphp
                            <span class="fw-bold fs-6 text-gray-800">{{$cnpj_cpf}}</span>
                        </div>
                    </div>
                    <div class="row mb-7">
                        <label class="col-lg-2 fw-semibold text-muted">Telefone Celular</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{$celular}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Telefone Auxiliar</label>
                        <div class="col-lg-2">
                        @php
                            $celularAux = substr_replace($associado->celular_aux, '(', 0, 0);
                            $celularAux = substr_replace($celularAux, '9', 3, 0);
                            $celularAux = substr_replace($celularAux, ')', 3, 0);
                            $celularAux = substr_replace($celularAux, '-', 9, 0);
                        @endphp
                            <span class="fw-bold fs-6 text-gray-800">{{$celularAux}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Telefone Fixo</label>
                        <div class="col-lg-2">
                        @php
                            $celularFixo = substr_replace($associado->telefone_fixo, '(', 0, 0);
                            $celularFixo = substr_replace($celularFixo, ')', 3, 0);
                            $celularFixo = substr_replace($celularFixo, '-', 9, 0);
                        @endphp
                            <span class="fw-bold fs-6 text-gray-800">{{$celularFixo}}</span>
                        </div>
                    </div>
                    <div class="row mb-7">
                        <label class="col-lg-2 fw-semibold text-muted">RG</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->rg}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Nascimento</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{date('d/m/Y',strtotime($associado->data_nascimento))}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Contrato</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{date('d/m/Y',strtotime($associado->created_at))}}</span>
                        </div>
                    </div>
                    <div class="separator my-10"></div>
                    <div class="row mb-7">
                        <label class="col-lg-1 fw-semibold text-muted">CEP</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->cep}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Logradouro</label>
                        <div class="col-lg-3">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->logradouro." ".$associado->numero}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Complemento</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->complemento}}</span>
                        </div>
                    </div>
                    <div class="row mb-7">
                        <label class="col-lg-2 fw-semibold text-muted">Bairro</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->bairro}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Cidade</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->cidade}}</span>
                        </div>
                        <label class="col-lg-2 fw-semibold text-muted">Estado</label>
                        <div class="col-lg-2">
                            <span class="fw-bold fs-6 text-gray-800">{{$associado->estado}}</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('script')

@endsection
