"use strict";
var KTSigninGeneral = (function () {
    var e, t, i;
    return {
        init: function () {
            (e = document.querySelector("#kt_sign_in_form")),
                (t = document.querySelector("#kt_sign_in_submit")),
                (i = FormValidation.formValidation(e, {
                    fields: {
                        email: { validators: { regexp: { regexp: /^[^\s@]+@[^\s@]+\.[^\s@]+$/, message: "Isso não é um e-mail válido, esta faltando alguma coisa." }, notEmpty: { message: "O e-mail é obrigatório" } } },
                        password: { validators: { notEmpty: { message: "A senha é obrigatória" } } },
                    },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }) },
                })),
                t.addEventListener("click", function (n) {
                    n.preventDefault(),
                        i.validate().then(function (i) {
                            "Valid" == i
                                ? (t.setAttribute("data-kt-indicator", "on"),
                                  (t.disabled = !0),
                                  setTimeout(function () {
                                    document.querySelector("#kt_sign_in_form").submit()

                                  }, 2e3))
                                : Swal.fire({
                                      text: "Desculpe, parece que alguns erros foram detectados, tente novamente.",
                                      icon: "error",
                                      buttonsStyling: !1,
                                      confirmButtonText: "Ok, Entendi!",
                                      customClass: { confirmButton: "btn btn-primary" },
                                  });
                        });
                });
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTSigninGeneral.init();
});
