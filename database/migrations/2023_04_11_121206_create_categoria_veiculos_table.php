<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo_tipo_veiculo')->unsigned()->nullable();
            $table->foreign('codigo_tipo_veiculo')->references('id')->on('tipoveiculo')->nullable();
            $table->string('descricao_categoria', 30);
            $table->boolean('padrao')->nullable()->default(false);
            $table->string('hinova_id', 10)->nullable();
            $table->string('situacao', 17)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria_veiculos');
    }
};
