<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('placa', 7)->nullable();
            $table->string('chassi', 20)->nullable();
            $table->string('renavam', 15)->nullable();
            $table->integer('codigo_associado')->unsigned()->nullable();
            $table->foreign('codigo_associado')->references('id')->on('associados')->nullable();
            $table->integer('codigo_tipo')->unsigned()->nullable();
            $table->foreign('codigo_tipo')->references('id')->on('tipoveiculo')->nullable();
            $table->integer('codigo_classificacao')->unsigned()->nullable();
            $table->foreign('codigo_classificacao')->references('id')->on('classificacaos')->nullable();
            $table->string('codigo_fipe',10)->nullable();
            $table->float('valor_fipe',10,2)->nullable();
            $table->integer('codigo_marca')->unsigned()->nullable();
            $table->foreign('codigo_marca')->references('id')->on('montadoras')->nullable();
            $table->integer('codigo_modelo')->unsigned()->nullable();
            $table->foreign('codigo_modelo')->references('id')->on('modelos')->nullable();
            $table->string('ano_fabricacao',4)->nullable();
            $table->string('ano_modelo',4)->nullable();
            $table->integer('codigo_combustivel')->unsigned()->nullable();
            $table->foreign('codigo_combustivel')->references('id')->on('combustivel')->nullable();
            $table->integer('codigo_cor')->unsigned()->nullable();
            $table->foreign('codigo_cor')->references('id')->on('cors')->nullable();
            $table->integer('codigo_categoria')->unsigned()->nullable();
            $table->foreign('codigo_categoria')->references('id')->on('categoria_veiculos')->nullable();
            $table->integer('codigo_voluntario')->unsigned()->nullable();
            $table->foreign('codigo_voluntario')->references('id')->on('voluntarios')->nullable();
            $table->date('data_contrato')->nullable();
            $table->string('hinova_id', 10)->nullable();
            $table->string('softruck_id', 40)->nullable();
            $table->string('situacao', 17)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculos');
    }
};
