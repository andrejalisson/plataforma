<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razaoSocial', 100);
            $table->string('nomeFantasia', 100)->nullable();
            $table->string('cnpj', 14);
            $table->string('email', 50)->nullable();
            $table->string('telefone', 20)->nullable();
            $table->string('celular', 20)->nullable();
            $table->string('centralRoubo', 20)->nullable();
            $table->string('CentralAssistencia', 20)->nullable();
            $table->string('cep', 8)->nullable();
            $table->string('logradouro', 100)->nullable();
            $table->string('numero', 6)->nullable();
            $table->string('complemento', 50)->nullable();
            $table->string('bairro', 50)->nullable();
            $table->string('cidade', 50)->nullable();
            $table->string('estado', 2)->nullable();
            $table->string('softruck_id', 40)->nullable();
            $table->text('observacao')->nullable()->default('Sem observações');
            $table->string('logo', 27)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
};
