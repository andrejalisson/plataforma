<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('usuario', 50)->nullable();
            $table->string('sexo', 1)->nullable();
            $table->string('tipo_pessoa', 8)->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('cpfcnpj', 14)->nullable();
            $table->string('rg', 50)->nullable();
            $table->string('telefone_fixo', 15)->nullable();
            $table->string('celular', 15)->nullable();
            $table->string('celular_aux', 15)->nullable();
            $table->string('email', 80)->nullable();
            $table->string('cep', 8)->nullable();
            $table->string('logradouro', 100)->nullable();
            $table->string('numero', 10)->nullable();
            $table->string('complemento', 100)->nullable();
            $table->string('bairro', 50)->nullable();
            $table->string('cidade', 50)->nullable();
            $table->string('estado', 2)->nullable();
            $table->string('softruck_id', 40)->nullable();
            $table->string('hinova_id', 10)->nullable();
            $table->date('data_contrato')->nullable();
            $table->integer('empresa_id')->unsigned()->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas')->nullable();
            $table->text('observacao')->nullable()->default('Sem observações');
            $table->string('situacao', 17)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados');
    }
};
