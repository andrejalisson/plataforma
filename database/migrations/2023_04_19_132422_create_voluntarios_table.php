<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voluntarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 100)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('cpfcnpj', 14)->nullable();
            $table->string('cep', 8)->nullable();
            $table->string('telefone', 11)->nullable();
            $table->string('telefone_comercial', 11)->nullable();
            $table->string('celular', 11)->nullable();
            $table->text('observacao')->nullable();
            $table->string('logradouro', 100)->nullable();
            $table->string('numero', 8)->nullable();
            $table->string('complemento', 100)->nullable();
            $table->string('bairro', 50)->nullable();
            $table->string('cidade', 50)->nullable();
            $table->string('estado', 15)->nullable();
            $table->string('hinova_id', 10)->nullable();
            $table->string('situacao', 17)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voluntarios');
    }
};
