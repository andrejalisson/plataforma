<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoveiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 30);
            $table->float('participacao_minima', 8,2);
            $table->float('porcentagem_fipe',  8,2);
            $table->boolean('exibirRateio')->nullable()->default(true);
            $table->boolean('cobrarRateio')->nullable()->default(true);
            $table->string('hinova_id', 10)->nullable();
            $table->string('situacao', 17)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipoveiculo');
    }
};
