<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cobrancas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nosso_numero', 10)->nullable();
            $table->integer('associado_id')->unsigned()->nullable();
            $table->foreign('associado_id')->references('id')->on('associados')->nullable();
            $table->string('codigo_boleto',10);
            $table->string('linha_digitavel', 60)->nullable();
            $table->string('link_boleto', 50)->nullable();
            $table->string('descricao_situacao_boleto', 20)->nullable();
            $table->float('valor_boleto')->default(0.00);
            $table->date('data_emissao');
            $table->date('data_vencimento');
            $table->date('data_vencimento_original');
            $table->date('data_pagamento')->nullable();
            $table->date('data_credito_banco')->nullable();
            $table->string('descricao_forma_pagamento', 20)->nullable();
            $table->string('descricao_tipo_cobranca_recorrente', 20)->nullable();
            $table->string('descricao_tipo_boleto', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cobrancas');
    }
};
