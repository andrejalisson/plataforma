<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao_modelo', 100)->nullable();
            $table->integer('codigo_montadora')->unsigned()->nullable();
            $table->foreign('codigo_montadora')->references('id')->on('montadoras')->nullable();
            $table->string('codigo_fipe',10)->nullable();
            $table->string('hinova_id', 10)->nullable();
            $table->string('situacao', 17)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelos');
    }
};
