<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AcessoController;
use App\Http\Controllers\IntegracaoController;
use App\Http\Controllers\AssociadoController;
use App\Http\Controllers\VeiculoController;
use App\Http\Controllers\DashBoardController;



Route::get('/', [AcessoController::class, 'login']);
Route::get('/Login', [AcessoController::class, 'login'])->name('login');
Route::post('/Verifica', [AcessoController::class, 'verifica']);
Route::get('/Sair', [AcessoController::class, 'logout']);

Route::get('/Integracao', [IntegracaoController::class, 'lista']);

Route::controller(AssociadoController::class)->group(function () {
    Route::get('/migrarSoftruck', 'softruck');
    Route::get('/Associado', 'listar');
    Route::get('/Associado/{id}', 'associado');
    Route::post('/todosAssociados', 'todosAssociados')->name('todosAssociados');
});

Route::controller(VeiculoController::class)->group(function () {
    Route::get('/Veiculos', 'lista');
    Route::post('/todosVeiculos', 'todosVeiculos')->name('todosVeiculos');
    Route::get('/Veiculos/{id}', 'veiculos');
    Route::get('/MigrarVeiculos/{id}', 'migrar');
    
});


Route::controller(DashBoardController::class)->group(function () {
    Route::get('/DashBoard/Geral', 'geral');
});
