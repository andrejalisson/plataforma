<?php

namespace App\Http\Controllers;

use App\Models\Veiculo;
use Illuminate\Http\Request;

class VeiculoController extends Controller{
    public function lista(){
        $title = "Veículos";
        return view('veiculos.listar')->with(compact('title'));
    }

    public function migrar($id){
        $veiculo = Veiculo::where('id', $id)->first();
        dd($veiculo);
        
    }

    public function veiculos($id){
        $title = "Veiculos";
        $veiculo = Veiculo::select('associados.nome as associado','associados.celular as associadoCelular','associados.id as associadoId','voluntarios.nome as consultor','voluntarios.celular as consultorCelular','veiculos.placa','veiculos.ano_fabricacao','veiculos.ano_modelo','cors.descricao_cor','veiculos.chassi','veiculos.renavam','montadoras.descricao','modelos.descricao_modelo','veiculos.situacao', 'classificacaos.descricao as rastreamento')
                            ->where('veiculos.id', '=', $id)
                            ->leftJoin('cors', 'veiculos.codigo_cor', '=', 'cors.id')
                            ->leftJoin('classificacaos', 'veiculos.codigo_classificacao', '=', 'classificacaos.id')
                            ->leftJoin('associados', 'veiculos.codigo_associado', '=', 'associados.id')
                            ->leftJoin('montadoras', 'veiculos.codigo_marca', '=', 'montadoras.id')
                            ->leftJoin('modelos', 'veiculos.codigo_modelo', '=', 'modelos.id')
                            ->leftJoin('voluntarios', 'veiculos.codigo_voluntario', '=', 'voluntarios.id')
                            ->first();
        return view('veiculos.view')->with(compact('title', 'veiculo'));
    }

    public function todosVeiculos(Request $request){
        $columns = array(
            0 =>'placa',
            1 =>'chassi',
            4 =>'situacao',
        );

        $totalData = Veiculo::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $veiculos =  Veiculo::select('associados.nome as associado','veiculos.id as veiculosId','associados.celular as associadoCelular','associados.id as associadoId','voluntarios.nome as consultor','voluntarios.celular as consultorCelular','veiculos.placa','veiculos.chassi','veiculos.renavam','montadoras.descricao','modelos.descricao_modelo','veiculos.situacao', 'classificacaos.descricao as rastreamento')
                                    ->leftJoin('classificacaos', 'veiculos.codigo_classificacao', '=', 'classificacaos.id')
                                    ->leftJoin('associados', 'veiculos.codigo_associado', '=', 'associados.id')
                                    ->leftJoin('montadoras', 'veiculos.codigo_marca', '=', 'montadoras.id')
                                    ->leftJoin('modelos', 'veiculos.codigo_modelo', '=', 'modelos.id')
                                    ->leftJoin('voluntarios', 'veiculos.codigo_voluntario', '=', 'voluntarios.id')
                                    ->offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $veiculos =  Veiculo::select('associados.nome as associado','veiculos.id as veiculosId','associados.celular as associadoCelular','associados.id as associadoId','voluntarios.nome as consultor','voluntarios.celular as consultorCelular','veiculos.placa','veiculos.chassi','veiculos.renavam','montadoras.descricao','modelos.descricao_modelo','veiculos.situacao', 'classificacaos.descricao as rastreamento')
                                    ->leftJoin('classificacaos', 'veiculos.codigo_classificacao', '=', 'classificacaos.id')
                                    ->leftJoin('associados', 'veiculos.codigo_associado', '=', 'associados.id')
                                    ->leftJoin('montadoras', 'veiculos.codigo_marca', '=', 'montadoras.id')
                                    ->leftJoin('modelos', 'veiculos.codigo_modelo', '=', 'modelos.id')
                                    ->leftJoin('voluntarios', 'veiculos.codigo_voluntario', '=', 'voluntarios.id')
                                    ->where('placa','LIKE',"%{$search}%")
                                    ->orwhere('chassi','LIKE',"%{$search}%")
                                    ->orwhere('voluntarios.nome','LIKE',"%{$search}%")
                                    ->orwhere('associados.nome','LIKE',"%{$search}%")
                                    ->orwhere('veiculos.situacao','LIKE',"%{$search}%")
                                    ->offset($start)
                                    ->limit($limit)
                                    ->orderBy($order,$dir)
                                    ->get();
            $totalFiltered =  Veiculo::select('associados.nome as associado','veiculos.id as veiculosId','associados.celular as associadoCelular','associados.id as associadoId','voluntarios.nome as consultor','voluntarios.celular as consultorCelular','veiculos.placa','veiculos.chassi','veiculos.renavam','montadoras.descricao','modelos.descricao_modelo','veiculos.situacao', 'classificacaos.descricao as rastreamento')
                                    ->leftJoin('classificacaos', 'veiculos.codigo_classificacao', '=', 'classificacaos.id')
                                    ->leftJoin('associados', 'veiculos.codigo_associado', '=', 'associados.id')
                                    ->leftJoin('montadoras', 'veiculos.codigo_marca', '=', 'montadoras.id')
                                    ->leftJoin('modelos', 'veiculos.codigo_modelo', '=', 'modelos.id')
                                    ->leftJoin('voluntarios', 'veiculos.codigo_voluntario', '=', 'voluntarios.id')
                                    ->where('placa','LIKE',"%{$search}%")
                                    ->orwhere('chassi','LIKE',"%{$search}%")
                                    ->orwhere('voluntarios.nome','LIKE',"%{$search}%")
                                    ->orwhere('associados.nome','LIKE',"%{$search}%")
                                    ->orwhere('veiculos.situacao','LIKE',"%{$search}%")
                                    ->count();
        }
        $data = array();

        if(!empty($veiculos)){
            foreach ($veiculos as $veiculoss){
                $nestedData['placa'] = "<div class=\"d-flex flex-column\">
                                            <a href=\"/Veiculos/".$veiculoss->veiculosId."\" class=\"text-gray-800 text-hover-primary mb-1\">".$veiculoss->placa."</a>
                                            <a href=\"/Veiculos/".$veiculoss->veiculosId."\" class=\"text-gray-800 text-hover-primary mb-1\">".$veiculoss->descricao." - ".$veiculoss->descricao_modelo."</a>
                                        </div>";
                $nestedData['chassi'] = "<div class=\"d-flex flex-column\">
                                            <a href=\"/Veiculos/".$veiculoss->veiculosId."\" target=\"_blank\" class=\"text-gray-800 text-hover-primary mb-1\">".$veiculoss->chassi."</a>
                                            <a href=\"/Veiculos/".$veiculoss->veiculosId."\" target=\"_blank\" class=\"text-gray-800 text-hover-primary mb-1\">".$veiculoss->renavam."</a>
                                        </div>";
                $nestedData['associado'] = "<div class=\"d-flex flex-column\">
                                            <a href=\"/Associado/".$veiculoss->associadoId."\" target=\"_blank\" class=\"text-gray-800 text-hover-primary mb-1\">".$veiculoss->associado."</a>
                                            <span>".$veiculoss->associadoCelular."</span>
                                        </div>";
                $nestedData['consultor'] = "<div class=\"d-flex flex-column\">
                                                <span>".$veiculoss->consultor."</span>
                                                <span>".$veiculoss->consultorCelular."</span>
                                            </div>";
                switch ($veiculoss->situacao) {
                    case 'ATIVO':
                        $status ="<span class=\"badge badge-success\">ATIVO</span>";
                        break;
                    case 'PENDENTE':
                        $status ="<span class=\"badge badge-warning\">PENDENTE</span>";
                        break;
                    case 'NEGADO':
                        $status ="<span class=\"badge badge-dark\">NEGADO</span>";
                        break;
                    case 'INATIVO/PAGAMENTO':
                        $status ="<span class=\"badge badge-danger\">INATIVO/PAGAMENTO</span>";
                        break;
                    case 'INATIVO':
                        $status ="<span class=\"badge badge-danger\">INATIVO</span>";
                        break;
                    case 'INADIMPLENTE':
                        $status ="<span class=\"badge badge-info\">INADIMPLENTE</span>";
                        break;

                    default:
                    $status =  $veiculoss->situacao;
                        break;
                }
                $nestedData['status'] = "<div class=\"d-flex flex-column\">
                                                <span>".$veiculoss->rastreamento."</span>
                                                <span>".$status."</span>
                                            </div>";

                $nestedData['opcoes'] = "<a href=\"/Veiculos/".$veiculoss->veiculosId."\" class=\"btn btn-icon btn-success\"><i class=\"fa-solid fa-eye\"></i></a>
                                            <a href=\"/MigrarVeiculos/".$veiculoss->veiculosId."\" class=\"btn btn-icon btn-secondary\"><i class=\"bi bi-arrow-down-up\"></i></a>
                                            <a href=\"#\" class=\"btn btn-icon btn-primary\"><i class=\"bi bi-chat-square-text-fill fs-4 me-2\"></i></a>";

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
