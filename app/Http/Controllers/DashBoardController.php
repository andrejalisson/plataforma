<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashBoardController extends Controller{
    public function geral(Request $request){
        $title = "DashBoard Geral";
        return view('dashboard.geral')->with(compact('title'));
    }
}
