<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class AcessoController extends Controller{
    public function login(Request $request){
        if (Auth::check()) {
            $request->session()->flash('sucesso', 'Bem vindo novamente!');
            return redirect('/DashBoard/Geral');
        }
        $title = "Login";
        return view('acesso.login')->with(compact('title'));
    }

    public function verifica(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], false)) {
            $request->session()->flash('sucesso', 'Bom trabalho!');
            return redirect('/DashBoard/Geral');
        }
        $request->session()->flash('atencao', 'Email ou senha incorretos.');
        return redirect()->back();
    }

    public function logout(Request $request){
        session()->flush();
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        $request->session()->flash('sucesso', 'Até Logo!');
        return redirect('/Login');
    }



}
