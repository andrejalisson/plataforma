<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Associado;
use App\Models\Empresa;

class AssociadoController extends Controller{
    public function softruck(){
        $usuarioHinova = Http::withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
            'codigo_situacao' => 1,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 5000,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*5000;
            $usuarioHinova = Http::withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
                'codigo_situacao' => 1,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 5000,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->associados as $associados) {
                $consultaAssociado = Associado::where('cpfcnpj', $associados->nome)->first();
                dd($consultaAssociado);
                if($consultaAssociado == null){
                    $associado = new Associado();
                }else{
                    $associado = $consultaAssociado;
                }
                $associado->nome = $associados->nome;
                $associado->usuario = $associados->cpf;
                $associado->sexo = $associados->sexo;
                $associado->data_nascimento = date('Y-m-d',strtotime($associados->data_nascimento));
                $associado->cpfcnpj = $associados->cpf;
                $associado->rg = $associados->rg_associado;
                $telefone_fixo = trim($associados->telefone);
                $telefone_fixo = str_replace("(", "", $telefone_fixo);
                $telefone_fixo = str_replace(")", "", $telefone_fixo);
                $telefone_fixo = str_replace(" ", "", $telefone_fixo);
                $telefone_fixo = str_replace("-", "", $telefone_fixo);
                $associado->telefone_fixo = $associados->ddd.$telefone_fixo;
                $telefone_celular = trim($associados->telefone_celular);
                $telefone_celular = str_replace("(", "", $telefone_celular);
                $telefone_celular = str_replace(")", "", $telefone_celular);
                $telefone_celular = str_replace(" ", "", $telefone_celular);
                $telefone_celular = str_replace("-", "", $telefone_celular);
                $associado->celular = $associados->ddd_celular.$telefone_celular;
                $telefone_celular_aux = trim($associados->telefone_celular_aux);
                $telefone_celular_aux = str_replace("(", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(")", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(" ", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace("-", "", $telefone_celular_aux);
                $associado->celular_aux = $associados->ddd_celular_aux.$telefone_celular_aux;
                $associado->email = $associados->email;
                $cep = trim($associados->cep);
                $cep = str_replace("-", "", $cep);
                $associado->cep = $cep;
                $associado->tipo_pessoa = $associados->tipo_pessoa;
                $associado->logradouro = $associados->logradouro;
                $associado->numero = $associados->numero;
                $associado->complemento = $associados->complemento;
                $associado->bairro = $associados->bairro;
                $associado->cidade = $associados->cidade;
                $associado->estado = $associados->estado;
                $associado->situacao = "ATIVO";
                $associado->hinova_id = $associados->codigo_associado;
                $associado->save();
                $this->info($associados->nome." - Cadastrado");
            }
            sleep(5);
        }

    }

    public function listar(){
        $title = "Associados";
        return view('associado.listar')->with(compact('title'));
    }

    public function associado($id){
        $title = "Associados";
        $associado = Associado::where('id', '=', $id)->first();
        return view('associado.view')->with(compact('title', 'associado'));
    }

    public function todosAssociados(Request $request){
        $columns = array(
            0 =>'nome',
            1 =>'celular',
            2 =>'situacao',
        );

        $totalData = Associado::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $associado = Associado::offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $associado =  Associado::where('nome','LIKE',"%{$search}%")
                                    ->orwhere('cpfCnpj','LIKE',"%{$search}%")
                                    ->orwhere('celular','LIKE',"%{$search}%")
                                    ->orwhere('situacao','LIKE',"%{$search}%")
                                    ->offset($start)
                                    ->limit($limit)
                                    ->orderBy($order,$dir)
                                    ->get();
            $totalFiltered = Associado::where('nome','LIKE',"%{$search}%")
                                    ->orwhere('cpfCnpj','LIKE',"%{$search}%")
                                    ->orwhere('celular','LIKE',"%{$search}%")
                                    ->orwhere('situacao','LIKE',"%{$search}%")
                                    ->count();
        }
        $data = array();

        if(!empty($associado)){
            foreach ($associado as $associado){
                $cnpj_cpf = preg_replace("/\D/", '', $associado->cpfcnpj);
                if (strlen($cnpj_cpf) === 11) {
                    $cnpj_cpf = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
                }else{
                    $cnpj_cpf = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                }


                $nestedData['nome'] = "<div class=\"d-flex flex-column\">
                                            <a href=\"/Associado/".$associado->id."\" class=\"text-gray-800 text-hover-primary mb-1\">".strtoupper($associado->nome)."</a>
                                            <span>".$cnpj_cpf."</span>
                                        </div>";

                                            $celular = substr_replace($associado->celular, '(', 0, 0);
                                            $celular = substr_replace($celular, '9', 3, 0);
                                            $celular = substr_replace($celular, ')', 3, 0);
                                            $celular = substr_replace($celular, '-', 9, 0);
                $nestedData['contato'] = "<div class=\"d-flex flex-column\">
                                            <a href=\"https://api.whatsapp.com/send?phone=55{$associado->celular}\" target=\"_blank\" class=\"text-gray-800 text-hover-primary mb-1\">".$celular."</a>
                                            <span>".$associado->email."</span>
                                        </div>";
                switch ($associado->situacao) {
                    case 'ATIVO':
                        $nestedData['status'] ="<span class=\"badge badge-success\">ATIVO</span>";
                        break;
                    case 'PENDENTE':
                        $nestedData['status'] ="<span class=\"badge badge-warning\">PENDENTE</span>";
                        break;
                    case 'NEGADO':
                        $nestedData['status'] ="<span class=\"badge badge-dark\">NEGADO</span>";
                        break;
                    case 'INATIVO/PAGAMENTO':
                        $nestedData['status'] ="<span class=\"badge badge-danger\">INATIVO/PAGAMENTO</span>";
                        break;
                    case 'INATIVO':
                        $nestedData['status'] ="<span class=\"badge badge-danger\">INATIVO</span>";
                        break;
                    case 'INADIMPLENTE':
                        $nestedData['status'] ="<span class=\"badge badge-info\">INADIMPLENTE</span>";
                        break;

                    default:
                    $nestedData['status'] =$associado->situacao;
                        break;
                }

                $nestedData['opcoes'] = "<a href=\"/Associado/".$associado->id."\" class=\"btn btn-icon btn-success\"><i class=\"fa-solid fa-eye\"></i></a>
                                            <a href=\"#\" class=\"btn btn-icon btn-secondary\"><i class=\"bi bi-chat-square-text-fill fs-4 me-2\"></i></a>
                                            <a href=\"#\" class=\"btn btn-icon btn-primary\"><i class=\"bi bi-chat-square-text-fill fs-4 me-2\"></i></a>";

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }

}
