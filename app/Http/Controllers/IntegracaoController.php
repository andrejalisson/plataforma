<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IntegracaoController extends Controller{
    public function lista(Request $request){
        $title = "Integrações";
        return view('configuracoes.listaIntegracao')->with(compact('title'));
    }
}
