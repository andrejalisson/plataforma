<?php

use Illuminate\Support\Facades\Http;


function wppTexto($mensagem, $numero, $data){
    switch ($data) {
        case 5:
            Http::post(env('API_WPP')."/rest/sendMessage/text/?id=".env('TOKEN05'), [
                'receiver' => $numero,
                'message' => [
                    'text' => $mensagem
                ]
            ]);
            break;
        
        default:
            # code...
            break;
    }
    
}

function wppLink($link, $descricao, $numero, $data){
    Http::post(env('API_WPP')."/rest/sendMessage/textlink/?id=".env('TOKEN_WPP'), [
        'receiver' => '85'.substr($numero,3),
        'message' => [
            'text' => $descricao." ".$link
        ]
    ]);
}
