<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipoveiculo extends Model
{
    use HasFactory;
    protected $table = 'tipoveiculo';
}
