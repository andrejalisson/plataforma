<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Veiculo;
use Illuminate\Support\Facades\Http;

class atualizarSoftruck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atualizar:softruck';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza os veículos da Softruck acordo com o SGA';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        $response = Http::accept('application/json')->withOptions(["verify"=>false])->post('https://public-api.softruck.com/api/v1/auth/login', [
            'username' => 'andrejalisson',
            'password' => 'Aa@31036700.',
        ]);
        $token = json_decode($response->body());
        $quantidade = 0;
        foreach ($token as $dados) {
            $tokenSoftruck = $dados->token;
        }
        $informacoes = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page=1');
        $json_str = $informacoes->body();
        $jsonObj = json_decode($json_str);
        $paginas = json_encode($jsonObj->data->total_pages);
        for ($i=1; $i < $paginas; $i++) { 
            $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
            if ($veiculo->status() == 429) {
                $this->info("TRAVOOU - AGUARDA 20 SEGUNDOS");
                sleep(20);    
                $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
                if ($veiculo->status() == 429) {
                    $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                    sleep(20);    
                    $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
                }
                if ($veiculo->status() == 429) {
                    $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                    sleep(20);    
                    $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
                }
                if ($veiculo->status() == 429) {
                    $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                    sleep(20);    
                    $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
                }
                if ($veiculo->status() == 429) {
                    $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                    sleep(20);    
                    $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
                }
            }
            $this->info($i." - PÁGINA");
            $this->info($veiculo->status()." - STATUS");
            $json_str = $veiculo->body();
            $jsonObj = json_decode($json_str);
            $veiculos = json_encode($jsonObj->data->rows);
            foreach (json_decode($veiculos) as $veiculos) {
                $placa = "";
                $chassi = "";
                if(isset($veiculos->plate)){
                    $placa = str_replace('-', '', $veiculos->plate);
                    $vei = Veiculo::where('placa', $placa)->first();
                }
                if(isset($veiculos->vin)){
                    $chassi = $veiculos->vin;
                    $vei = Veiculo::where('chassi', $veiculos->vin)->first();
                }
                if($vei == null){                    
                }else{
                    $data = array(
                        "data" => array(
                            "plate" => $placa,
                            "vin" => $chassi,
                            "registration_no" => $vei->renavam,
                            "type" => $veiculos->type
                        )
                    );
                    $quantidade++;
                    $empresas = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa, $data);
                    if ($empresas->status() == 429) {
                        $this->info("TRAVOOU - AGUARDA 20 SEGUNDOS");
                        sleep(20);    
                        $empresas = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa, $data);
                        if ($empresas->status() == 429) {
                            $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                            sleep(20);    
                            $empresas = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa, $data);
                        }
                        if ($empresas->status() == 429) {
                            $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                            sleep(20);    
                            $empresas = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa, $data);
                        }
                        if ($empresas->status() == 429) {
                            $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                            sleep(20);    
                            $empresas = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa, $data);
                        }
                        if ($empresas->status() == 429) {
                            $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                            sleep(20);    
                            $empresas = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa, $data);
                        }
                    }
                    
                    
                    
                    switch ($vei->situacao) {
                        case 'ATIVO':
                            $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            $this->info("VEÍCULO ATIVO, ACESSO LIBERADO");
                            break;
                        case 'PENDENTE':
                            $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/activation');
                            }
                            $this->info("VEÍCULO PENDENTE, ACESSO LIBERADO");
                            break;
                        case 'INATIVO':
                            $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            $this->info("VEÍCULO INATIVO, ACESSO BLOQUEADO");
                            break;
                        case 'INATIVO/PAGAMENTO':
                            $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            $this->info("VEÍCULO INATIVO/PAGAMENTO, ACESSO BLOQUEADO");
                            break;
                        case 'INADIMPLENTE':
                            $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            $this->info("VEÍCULO INADIMPLENTE, ACESSO BLOQUEADO");
                            break;
                        case 'NEGADO':
                            $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU NOVAMENTE- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU MAIS UMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU ARRIÉGUA, SERÁ QUE NÃO VAI?- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            if ($status->status() == 429) {
                                $this->info("TRAVOOU VOU TENTAR A ULTIMA VEZ- AGUARDA 20 SEGUNDOS");
                                sleep(20);    
                                $status = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa.'/toggle-activation/deactivation');
                            }
                            $this->info("VEÍCULO NEGADO, ACESSO BLOQUEADO");
                            break;
                        default:
                        $this->info("VEÍCULO $vei->situacao, NENHUMA ALTERAÇÃO FEITA NO ACESSO");
                            break;
                    }                    
                    $this->info($quantidade." - ".$placa." - ".$chassi." - ATUALIZADO");
                }

            }
            sleep(10);
        }




        return Command::SUCCESS;
    }
}
