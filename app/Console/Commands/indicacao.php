<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Associado;

class indicacao extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gerar:indicacao';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        $associado = Associado::get();
        foreach ($associado as $associado) {
            if ($associado->id > 4511) {
                $id = $associado->id - 1;
                $indicador = Associado::where('id', $id)->first();
                $indicacao = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/indicacao-externa/cadastrar',[
                    'nome' => $associado->nome,
                    'cpf' => $associado->cpfcnpj,
                    'ddd_telefone' => substr($associado->celular,0,2),
                    'telefone' => substr($associado->celular,2,9),
                    'codigo_associado_indicador' => $indicador->hinova_id,
                ]);
                $this->info($associado->nome." INDICADO POR -> ".$indicador->nome);
            }
        }
        



        return Command::SUCCESS;
    }
}
