<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Associado;

class migrarUsuarios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrar:AssociadoSGA';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
            'codigo_situacao' => 1,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 5000,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*5000;
            $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
                'codigo_situacao' => 1,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 5000,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->associados as $associados) {
                $consultaAssociado = Associado::where('cpfcnpj', $associados->cpf)->first();
                if($consultaAssociado == null){
                    $associado = new Associado();
                }else{
                    $associado = $consultaAssociado;
                }
                $associado->nome = $associados->nome;
                $associado->usuario = $associados->cpf;
                $associado->sexo = $associados->sexo;
                $associado->data_nascimento = date('Y-m-d',strtotime($associados->data_nascimento));
                $associado->cpfcnpj = $associados->cpf;
                $associado->rg = $associados->rg_associado;
                $telefone_fixo = trim($associados->telefone);
                $telefone_fixo = str_replace("(", "", $telefone_fixo);
                $telefone_fixo = str_replace(")", "", $telefone_fixo);
                $telefone_fixo = str_replace(" ", "", $telefone_fixo);
                $telefone_fixo = str_replace("-", "", $telefone_fixo);
                $associado->telefone_fixo = $associados->ddd.$telefone_fixo;
                $telefone_celular = trim($associados->telefone_celular);
                $telefone_celular = str_replace("(", "", $telefone_celular);
                $telefone_celular = str_replace(")", "", $telefone_celular);
                $telefone_celular = str_replace(" ", "", $telefone_celular);
                $telefone_celular = str_replace("-", "", $telefone_celular);
                $associado->celular = $associados->ddd_celular.$telefone_celular;
                $telefone_celular_aux = trim($associados->telefone_celular_aux);
                $telefone_celular_aux = str_replace("(", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(")", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(" ", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace("-", "", $telefone_celular_aux);
                $associado->celular_aux = $associados->ddd_celular_aux.$telefone_celular_aux;
                $associado->email = $associados->email;
                $cep = trim($associados->cep);
                $cep = str_replace("-", "", $cep);
                $associado->cep = $cep;
                $associado->tipo_pessoa = $associados->tipo_pessoa;
                $associado->logradouro = $associados->logradouro;
                $associado->numero = $associados->numero;
                $associado->complemento = $associados->complemento;
                $associado->bairro = $associados->bairro;
                $associado->cidade = $associados->cidade;
                $associado->estado = $associados->estado;
                $associado->situacao = "ATIVO";
                $associado->hinova_id = $associados->codigo_associado;
                if ($associados->data_contrato_associado =! "0000-00-00") {
                    $associado->data_contrato = $associados->data_contrato_associado;
                }
                $associado->save();
                $this->info($associados->nome." - Cadastrado");
            }
            sleep(5);
        }


        $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
            'codigo_situacao' => 2,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 5000,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*5000;
            $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
                'codigo_situacao' => 2,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 5000,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->associados as $associados) {
                $consultaAssociado = Associado::where('cpfcnpj', $associados->cpf)->first();
                if($consultaAssociado == null){
                    $associado = new Associado();
                }else{
                    $associado = $consultaAssociado;
                }
                $associado->nome = $associados->nome;
                $associado->usuario = $associados->cpf;
                $associado->sexo = $associados->sexo;
                $associado->data_nascimento = date('Y-m-d',strtotime($associados->data_nascimento));
                $associado->cpfcnpj = $associados->cpf;
                $associado->rg = $associados->rg_associado;
                $telefone_fixo = trim($associados->telefone);
                $telefone_fixo = str_replace("(", "", $telefone_fixo);
                $telefone_fixo = str_replace(")", "", $telefone_fixo);
                $telefone_fixo = str_replace(" ", "", $telefone_fixo);
                $telefone_fixo = str_replace("-", "", $telefone_fixo);
                $associado->telefone_fixo = $associados->ddd.$telefone_fixo;
                $telefone_celular = trim($associados->telefone_celular);
                $telefone_celular = str_replace("(", "", $telefone_celular);
                $telefone_celular = str_replace(")", "", $telefone_celular);
                $telefone_celular = str_replace(" ", "", $telefone_celular);
                $telefone_celular = str_replace("-", "", $telefone_celular);
                $associado->celular = $associados->ddd_celular.$telefone_celular;
                $telefone_celular_aux = trim($associados->telefone_celular_aux);
                $telefone_celular_aux = str_replace("(", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(")", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(" ", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace("-", "", $telefone_celular_aux);
                $associado->celular_aux = $associados->ddd_celular_aux.$telefone_celular_aux;
                $associado->email = $associados->email;
                $cep = trim($associados->cep);
                $cep = str_replace("-", "", $cep);
                $associado->cep = $cep;
                $associado->tipo_pessoa = $associados->tipo_pessoa;
                $associado->logradouro = $associados->logradouro;
                $associado->numero = $associados->numero;
                $associado->complemento = $associados->complemento;
                $associado->bairro = $associados->bairro;
                $associado->cidade = $associados->cidade;
                $associado->estado = $associados->estado;
                $associado->situacao = "INATIVO";
                $associado->hinova_id = $associados->codigo_associado;
                if ($associados->data_contrato_associado =! "0000-00-00") {
                    $associado->data_contrato = $associados->data_contrato_associado;
                }
                $associado->save();
                $this->info($associados->nome." - Cadastrado");
            }
            sleep(5);
        }

        $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
            'codigo_situacao' => 3,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 5000,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*5000;
            $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
                'codigo_situacao' => 3,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 5000,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->associados as $associados) {
                $consultaAssociado = Associado::where('cpfcnpj', $associados->cpf)->first();
                if($consultaAssociado == null){
                    $associado = new Associado();
                }else{
                    $associado = $consultaAssociado;
                }
                $associado->nome = $associados->nome;
                $associado->usuario = $associados->cpf;
                $associado->sexo = $associados->sexo;
                $associado->data_nascimento = date('Y-m-d',strtotime($associados->data_nascimento));
                $associado->cpfcnpj = $associados->cpf;
                $associado->rg = $associados->rg_associado;
                $telefone_fixo = trim($associados->telefone);
                $telefone_fixo = str_replace("(", "", $telefone_fixo);
                $telefone_fixo = str_replace(")", "", $telefone_fixo);
                $telefone_fixo = str_replace(" ", "", $telefone_fixo);
                $telefone_fixo = str_replace("-", "", $telefone_fixo);
                $associado->telefone_fixo = $associados->ddd.$telefone_fixo;
                $telefone_celular = trim($associados->telefone_celular);
                $telefone_celular = str_replace("(", "", $telefone_celular);
                $telefone_celular = str_replace(")", "", $telefone_celular);
                $telefone_celular = str_replace(" ", "", $telefone_celular);
                $telefone_celular = str_replace("-", "", $telefone_celular);
                $associado->celular = $associados->ddd_celular.$telefone_celular;
                $telefone_celular_aux = trim($associados->telefone_celular_aux);
                $telefone_celular_aux = str_replace("(", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(")", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(" ", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace("-", "", $telefone_celular_aux);
                $associado->celular_aux = $associados->ddd_celular_aux.$telefone_celular_aux;
                $associado->email = $associados->email;
                $cep = trim($associados->cep);
                $cep = str_replace("-", "", $cep);
                $associado->cep = $cep;
                $associado->tipo_pessoa = $associados->tipo_pessoa;
                $associado->logradouro = $associados->logradouro;
                $associado->numero = $associados->numero;
                $associado->complemento = $associados->complemento;
                $associado->bairro = $associados->bairro;
                $associado->cidade = $associados->cidade;
                $associado->estado = $associados->estado;
                $associado->situacao = "PENDENTE";
                $associado->hinova_id = $associados->codigo_associado;
                if ($associados->data_contrato_associado =! "0000-00-00") {
                    $associado->data_contrato = $associados->data_contrato_associado;
                }
                $associado->save();
                $this->info($associados->nome." - Cadastrado");
            }
            sleep(5);
        }

        $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
            'codigo_situacao' => 4,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 5000,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*5000;
            $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
                'codigo_situacao' => 4,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 5000,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->associados as $associados) {
                $consultaAssociado = Associado::where('cpfcnpj', $associados->cpf)->first();
                if($consultaAssociado == null){
                    $associado = new Associado();
                }else{
                    $associado = $consultaAssociado;
                }
                $associado->nome = $associados->nome;
                $associado->usuario = $associados->cpf;
                $associado->sexo = $associados->sexo;
                $associado->data_nascimento = date('Y-m-d',strtotime($associados->data_nascimento));
                $associado->cpfcnpj = $associados->cpf;
                $associado->rg = $associados->rg_associado;
                $telefone_fixo = trim($associados->telefone);
                $telefone_fixo = str_replace("(", "", $telefone_fixo);
                $telefone_fixo = str_replace(")", "", $telefone_fixo);
                $telefone_fixo = str_replace(" ", "", $telefone_fixo);
                $telefone_fixo = str_replace("-", "", $telefone_fixo);
                $associado->telefone_fixo = $associados->ddd.$telefone_fixo;
                $telefone_celular = trim($associados->telefone_celular);
                $telefone_celular = str_replace("(", "", $telefone_celular);
                $telefone_celular = str_replace(")", "", $telefone_celular);
                $telefone_celular = str_replace(" ", "", $telefone_celular);
                $telefone_celular = str_replace("-", "", $telefone_celular);
                $associado->celular = $associados->ddd_celular.$telefone_celular;
                $telefone_celular_aux = trim($associados->telefone_celular_aux);
                $telefone_celular_aux = str_replace("(", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(")", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(" ", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace("-", "", $telefone_celular_aux);
                $associado->celular_aux = $associados->ddd_celular_aux.$telefone_celular_aux;
                $associado->email = $associados->email;
                $cep = trim($associados->cep);
                $cep = str_replace("-", "", $cep);
                $associado->cep = $cep;
                $associado->tipo_pessoa = $associados->tipo_pessoa;
                $associado->logradouro = $associados->logradouro;
                $associado->numero = $associados->numero;
                $associado->complemento = $associados->complemento;
                $associado->bairro = $associados->bairro;
                $associado->cidade = $associados->cidade;
                $associado->estado = $associados->estado;
                $associado->situacao = "INADIMPLENTE";
                $associado->hinova_id = $associados->codigo_associado;
                if ($associados->data_contrato_associado =! "0000-00-00") {
                    $associado->data_contrato = $associados->data_contrato_associado;
                }
                $associado->save();
                $this->info($associados->nome." - Cadastrado");
            }
            sleep(5);
        }

        $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
            'codigo_situacao' => 5,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 5000,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*5000;
            $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
                'codigo_situacao' => 5,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 5000,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->associados as $associados) {
                $consultaAssociado = Associado::where('cpfcnpj', $associados->cpf)->first();
                if($consultaAssociado == null){
                    $associado = new Associado();
                }else{
                    $associado = $consultaAssociado;
                }
                $associado->nome = $associados->nome;
                $associado->usuario = $associados->cpf;
                $associado->sexo = $associados->sexo;
                $associado->data_nascimento = date('Y-m-d',strtotime($associados->data_nascimento));
                $associado->cpfcnpj = $associados->cpf;
                $associado->rg = $associados->rg_associado;
                $telefone_fixo = trim($associados->telefone);
                $telefone_fixo = str_replace("(", "", $telefone_fixo);
                $telefone_fixo = str_replace(")", "", $telefone_fixo);
                $telefone_fixo = str_replace(" ", "", $telefone_fixo);
                $telefone_fixo = str_replace("-", "", $telefone_fixo);
                $associado->telefone_fixo = $associados->ddd.$telefone_fixo;
                $telefone_celular = trim($associados->telefone_celular);
                $telefone_celular = str_replace("(", "", $telefone_celular);
                $telefone_celular = str_replace(")", "", $telefone_celular);
                $telefone_celular = str_replace(" ", "", $telefone_celular);
                $telefone_celular = str_replace("-", "", $telefone_celular);
                $associado->celular = $associados->ddd_celular.$telefone_celular;
                $telefone_celular_aux = trim($associados->telefone_celular_aux);
                $telefone_celular_aux = str_replace("(", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(")", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(" ", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace("-", "", $telefone_celular_aux);
                $associado->celular_aux = $associados->ddd_celular_aux.$telefone_celular_aux;
                $associado->email = $associados->email;
                $cep = trim($associados->cep);
                $cep = str_replace("-", "", $cep);
                $associado->cep = $cep;
                $associado->tipo_pessoa = $associados->tipo_pessoa;
                $associado->logradouro = $associados->logradouro;
                $associado->numero = $associados->numero;
                $associado->complemento = $associados->complemento;
                $associado->bairro = $associados->bairro;
                $associado->cidade = $associados->cidade;
                $associado->estado = $associados->estado;
                $associado->situacao = "NEGADO";
                $associado->hinova_id = $associados->codigo_associado;
                if ($associados->data_contrato_associado =! "0000-00-00") {
                    $associado->data_contrato = $associados->data_contrato_associado;
                }
                $associado->save();
                $this->info($associados->nome." - Cadastrado");
            }
            sleep(5);
        }

        $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
            'codigo_situacao' => 6,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 5000,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*5000;
            $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/associado',[
                'codigo_situacao' => 6,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 5000,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->associados as $associados) {
                $consultaAssociado = Associado::where('cpfcnpj', $associados->cpf)->first();
                if($consultaAssociado == null){
                    $associado = new Associado();
                }else{
                    $associado = $consultaAssociado;
                }
                $associado->nome = $associados->nome;
                $associado->usuario = $associados->cpf;
                $associado->sexo = $associados->sexo;
                $associado->data_nascimento = date('Y-m-d',strtotime($associados->data_nascimento));
                $associado->cpfcnpj = $associados->cpf;
                $associado->rg = $associados->rg_associado;
                $telefone_fixo = trim($associados->telefone);
                $telefone_fixo = str_replace("(", "", $telefone_fixo);
                $telefone_fixo = str_replace(")", "", $telefone_fixo);
                $telefone_fixo = str_replace(" ", "", $telefone_fixo);
                $telefone_fixo = str_replace("-", "", $telefone_fixo);
                $associado->telefone_fixo = $associados->ddd.$telefone_fixo;
                $telefone_celular = trim($associados->telefone_celular);
                $telefone_celular = str_replace("(", "", $telefone_celular);
                $telefone_celular = str_replace(")", "", $telefone_celular);
                $telefone_celular = str_replace(" ", "", $telefone_celular);
                $telefone_celular = str_replace("-", "", $telefone_celular);
                $associado->celular = $associados->ddd_celular.$telefone_celular;
                $telefone_celular_aux = trim($associados->telefone_celular_aux);
                $telefone_celular_aux = str_replace("(", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(")", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace(" ", "", $telefone_celular_aux);
                $telefone_celular_aux = str_replace("-", "", $telefone_celular_aux);
                $associado->celular_aux = $associados->ddd_celular_aux.$telefone_celular_aux;
                $associado->email = $associados->email;
                $cep = trim($associados->cep);
                $cep = str_replace("-", "", $cep);
                $associado->cep = $cep;
                $associado->tipo_pessoa = $associados->tipo_pessoa;
                $associado->logradouro = $associados->logradouro;
                $associado->numero = $associados->numero;
                $associado->complemento = $associados->complemento;
                $associado->bairro = $associados->bairro;
                $associado->cidade = $associados->cidade;
                $associado->estado = $associados->estado;
                $associado->situacao = "INATIVO/PAGAMENTO";
                $associado->hinova_id = $associados->codigo_associado;
                if ($associados->data_contrato_associado =! "0000-00-00") {
                    $associado->data_contrato = $associados->data_contrato_associado;
                }
                $associado->save();
                $this->info($associados->nome." - Cadastrado");
            }
            sleep(5);
        }
        return Command::SUCCESS;
    }
}
