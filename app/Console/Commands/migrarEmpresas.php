<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Empresa;

class migrarEmpresas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrar:empresa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'migrar:empresa';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        $response = Http::accept('application/json')->post('https://public-api.softruck.com/api/v1/auth/login', [
            'username' => 'andrejalisson',
            'password' => 'Aa@31036700.',
        ]);
        $token = json_decode($response->body());
        foreach ($token as $dados) {
            $tokenSoftruck = $dados->token;
        }
        $empresa = Http::withToken($tokenSoftruck)->get(env('API_SOFTRUCK').'/enterprises');
        $json_str = $empresa->body();
        $jsonObj = json_decode($json_str);
        $empresas = json_encode($jsonObj->data->rows);
        foreach (json_decode($empresas) as $empresas) {
            $enterprise = Empresa::where('cnpj', $empresas->cnpj)->first();

            if($enterprise == null){
                $empresa = new Empresa();
                $empresa->razaoSocial = $empresas->name;
                $empresa->nomeFantasia = $empresas->fantasy_name;
                $empresa->cnpj = $empresas->cnpj;
                $empresa->softruck_id = $empresas->uuid;
                $empresa->email = $empresas->email;
                $empresa->telefone = $empresas->phone1;
                $empresa->centralRoubo = $empresas->theft_emergency_tel;
                $empresa->CentralAssistencia = $empresas->assistance_emergency_tel;
                $empresa->save();
                $this->info($empresas->fantasy_name." - cadastrada");
                sleep(1);
            }else{
                $empresa = $enterprise;
                $empresa->razaoSocial = $empresas->name;
                $empresa->nomeFantasia = $empresas->fantasy_name;
                $empresa->cnpj = $empresas->cnpj;
                $empresa->softruck_id = $empresas->uuid;
                $empresa->email = $empresas->email;
                $empresa->telefone = $empresas->phone1;
                $empresa->centralRoubo = $empresas->theft_emergency_tel;
                $empresa->CentralAssistencia = $empresas->assistance_emergency_tel;
                $empresa->save();
                $this->info("Empresa <b>".$empresas->fantasy_name."</b> Já estava cadastrada");
                sleep(1);
            }
        }
        return Command::SUCCESS;
    }
}
