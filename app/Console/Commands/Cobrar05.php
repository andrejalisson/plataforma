<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Associado;
use App\Models\Veiculo;
use App\Models\Cobranca;
use App\Models\BoletoVeiculo;


class Cobrar05 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cobrar:data05';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        $dias = date('Y-m-05');
        $fatura = Cobranca::Join('associados', 'associados.id', '=', 'cobrancas.associado_id')->select('cobrancas.id as idcobranca', 'nome', 'celular')->whereDate('data_vencimento', "=" , "2023-06-05")->where('descricao_situacao_boleto', "ABERTO")->get();
        foreach ($fatura as $fatura) {
            if ($fatura->idcobranca > 1573) {
                $wpp = "*S.GROUP BENEFICIOS*\n\nCARO *".$fatura->nome."*\n🚨⚠️ATENÇÃO\nCONSTA NO NOSSO SISTEMA BOLETO EM ABERTO PODERIA NOS DA RETORNO SOBRE O PAGAMENTO?\n➡️ Chave Pix é CNPJ: 43017947000118 Necessário o envio do comprovante";
                Http::post(env('API_WPP')."/rest/sendMessage/text/?id=".env('TOKEN05'), [
                    'receiver' => $fatura->celular, 
                    'message' => [
                        'text' => $wpp
                    ]
                ]);
                $this->info("Mensagem enviada: ". $fatura->nome);
            }
        }
        return Command::SUCCESS;
    }
}
