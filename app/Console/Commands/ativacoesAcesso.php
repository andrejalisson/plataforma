<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ativacoesAcesso extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::accept('application/json')->withOptions(["verify"=>false])->post('https://public-api.softruck.com/api/v1/auth/login', [
            'username' => 'andrejalisson',
            'password' => 'Aa@31036700.',
        ]);
        $token = json_decode($response->body());
        $quantidade = 0;
        foreach ($token as $dados) {
            $tokenSoftruck = $dados->token;
        }
        $informacoes = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page=1');
        $json_str = $informacoes->body();
        $jsonObj = json_decode($json_str);
        $paginas = json_encode($jsonObj->data->total_pages);
        for ($i=1; $i < $paginas; $i++) { 
            $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
            if ($veiculo->status() == 429) {
                $this->info("TRAVOOU - AGUARDA 20 SEGUNDOS");
                sleep(20);    
                $veiculo = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->get(env('API_SOFTRUCK').'/vehicles?limit=20&page='.$i);
            }
            $this->info($i." - PÁGINA");
            $this->info($veiculo->status()." - STATUS");
            $json_str = $veiculo->body();
            $jsonObj = json_decode($json_str);
            $veiculos = json_encode($jsonObj->data->rows);
            foreach (json_decode($veiculos) as $veiculos) {
                $placa = "";
                $chassi = "";
                if(isset($veiculos->plate)){
                    $placa = str_replace('-', '', $veiculos->plate);
                    $vei = Veiculo::where('placa', $placa)->first();
                }
                if(isset($veiculos->vin)){
                    $chassi = $veiculos->vin;
                    $vei = Veiculo::where('chassi', $veiculos->vin)->first();
                }
                if($vei == null){                    
                }else{
                    $data = array(
                        "data" => array(
                            "plate" => $placa,
                            "vin" => $chassi,
                            "registration_no" => $vei->renavam,
                            "type" => $veiculos->type
                        )
                    );
                    $quantidade++;
                    $empresas = Http::withToken($tokenSoftruck)->withOptions(["verify"=>false])->patch(env('API_SOFTRUCK').'/vehicles/'.$vei->placa, $data);
                    $this->info($quantidade." - ".$placa." - ".$chassi." - ATUALIZADO");
                }
            }
            sleep(10);
        }




        return Command::SUCCESS;
    }
}
