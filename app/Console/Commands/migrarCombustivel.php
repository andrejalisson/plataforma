<?php

namespace App\Console\Commands;

use App\Models\Classificacao;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Combustivel;
use App\Models\Cor;
use App\Models\Tipoveiculo;
use App\Models\Montadora;
use App\Models\Modelo;
use App\Models\Voluntario;

class migrarCombustivel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrar:confiVeiculos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        $combustivel = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->get(env('API_HINOVA').'/listar/combustivel/todos');
        $json_str = $combustivel->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $combustiveis) {
            $consultaCombustivel = Combustivel::where('hinova_id', $combustiveis->codigo_combustivel)->first();
                if($consultaCombustivel == null){
                    $combistivel = new Combustivel();
                }else{
                    $combistivel = $consultaCombustivel;
                }
            $combistivel->descricao = $combustiveis->descricao;
            $combistivel->situacao = $combustiveis->situacao;
            $combistivel->hinova_id = $combustiveis->codigo_combustivel;
            $combistivel->save();
            $this->info($combistivel->descricao." - Cadastrado");
        }
        $tipo = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->get(env('API_HINOVA').'/listar/tipo-veiculo/todos');
        $json_str = $tipo->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $tipos) {
            $consultaTipo = Tipoveiculo::where('hinova_id', $tipos->codigo_tipo)->first();
                if($consultaTipo == null){
                    $tipo = new Tipoveiculo();
                }else{
                    $tipo = $consultaTipo;
                }
            $tipo->descricao = $tipos->descricao_tipo;
            $tipo->participacao_minima = $tipos->participacao_minima;
            $tipo->porcentagem_fipe = $tipos->porcentagem_fipe;
            $tipo->situacao = $tipos->situacao;
            $tipo->hinova_id = $tipos->codigo_tipo;
            $tipo->save();
            $this->info($tipos->descricao_tipo." - Cadastrado");
        }

        $montadora = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->get(env('API_HINOVA').'/listar/marca/todos');
        $json_str = $montadora->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $montadoras) {
            $consultaMontadora = Montadora::where('hinova_id', $montadoras->codigo_marca)->first();
                if($consultaMontadora == null){
                    $montadorass = new Montadora();
                }else{
                    $montadorass = $consultaMontadora;
                }
            $montadorass->descricao = $montadoras->descricao;
            $montadorass->situacao = $montadoras->situacao;
            $montadorass->hinova_id = $montadoras->codigo_marca;
            $montadorass->save();
            $this->info($montadoras->descricao." - Cadastrado");
        }

        $cor = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->get(env('API_HINOVA').'/listar/cor/todos');
        $json_str = $cor->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $cores) {
            $consultaCor = Cor::where('hinova_id', $cores->codigo_cor)->first();
                if($consultaCor == null){
                    $corr = new Cor();
                }else{
                    $corr = $consultaCor;
                }
            $corr->descricao_cor = $cores->descricao;
            $corr->situacao = $cores->situacao;
            $corr->hinova_id = $cores->codigo_cor;
            $corr->save();
            $this->info($cores->descricao." - Cadastrado");
        }

        $voluntarios = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->get(env('API_HINOVA').'/listar/voluntario/todos');
        $json_str = $voluntarios->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $voluntario) {
            $consultaVoluntario = Voluntario::where('hinova_id', $voluntario->codigo_voluntario)->first();
                if($consultaVoluntario == null){
                    $volunta = new Voluntario();
                }else{
                    $volunta = $consultaVoluntario;
                }
            $volunta->nome = $voluntario->nome;
            $volunta->cpfcnpj = $voluntario->cpf;
            $cep = trim($voluntario->cep);
            $cep = str_replace("-", "", $cep);
            $volunta->cep = $cep;
            $telefone = trim($voluntario->telefone);
            $telefone = str_replace("(", "", $telefone);
            $telefone = str_replace(")", "", $telefone);
            $telefone = str_replace(" ", "", $telefone);
            $telefone = str_replace("-", "", $telefone);
            $volunta->telefone = $telefone;
            $telefone_comercial = trim($voluntario->telefone_comercial);
            $telefone_comercial = str_replace("(", "", $telefone_comercial);
            $telefone_comercial = str_replace(")", "", $telefone_comercial);
            $telefone_comercial = str_replace(" ", "", $telefone_comercial);
            $telefone_comercial = str_replace("-", "", $telefone_comercial);
            $volunta->telefone_comercial = $telefone_comercial;
            $celular = trim($voluntario->celular);
            $celular = str_replace("(", "", $celular);
            $celular = str_replace(")", "", $celular);
            $celular = str_replace(" ", "", $celular);
            $celular = str_replace("-", "", $celular);
            $volunta->celular = $celular;
            $volunta->email = $voluntario->email;
            $volunta->situacao = $voluntario->situacao;
            $volunta->observacao = $voluntario->obs;
            $volunta->logradouro = $voluntario->logradouro;
            $volunta->numero = $voluntario->numero;
            $volunta->complemento = $voluntario->complemento;
            $volunta->bairro = $voluntario->bairro;
            $volunta->cidade = $voluntario->cidade;
            $volunta->estado = $voluntario->estado;
            $volunta->hinova_id = $voluntario->codigo_voluntario;
            $volunta->save();
            $this->info($volunta->nome." - Cadastrado");
        }

        $classificacao = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->get(env('API_HINOVA').'/listar/classificacao-veiculo/todos');
        $json_str = $classificacao->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $classificacoes) {
            $consultaClassificacao = Classificacao::where('hinova_id', $classificacoes->codigo)->first();
                if($consultaClassificacao == null){
                    $classif = new Classificacao();
                }else{
                    $classif = $consultaClassificacao;
                }
            $classif->descricao = $classificacoes->descricao;
            $classif->situacao = $classificacoes->situacao;
            $classif->hinova_id = $classificacoes->codigo;
            $classif->save();
            $this->info($classificacoes->descricao." - Cadastrado");
        }

        $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/modelo/listar',[
            'situacao' => "todos",
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 200,
        ]);
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*200;
            $usuarioHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/modelo/listar',[
                'situacao' => "todos",
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 200,
            ]);
            $json_str = $usuarioHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->modelos as $modelos) {
                $consultaModelo = Modelo::where('hinova_id', $modelos->codigo_modelo)->first();
                if($consultaModelo == null){
                    $modelo = new Modelo();
                }else{
                    $modelo = $consultaModelo;
                }
                $modelo->descricao_modelo = $modelos->descricao_modelo;
                $modelo->hinova_id = $modelos->codigo_modelo;
                $consultaMontadora = Montadora::where('hinova_id', $modelos->codigo_marca)->first();
                $modelo->codigo_montadora = $consultaMontadora->id;
                $modelo->situacao = $modelos->situacao;
                $modelo->save();
                $this->info($modelos->descricao_modelo." - Cadastrado");
            }
            sleep(5);
        }

        return Command::SUCCESS;
    }
}
