<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Associado;
use App\Models\Veiculo;
use App\Models\Cobranca;
use App\Models\BoletoVeiculo;

class MigrarBoletos30 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrar:boletos30';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/boleto',[
            'data_vencimento_inicial' => "26/".date("m/Y"),
            'data_vencimento_final' => date("t/m/Y"),
        ]);
        $json_str = $veiculosHinova->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $boleto) {
            $associado = Associado::where('hinova_id', $boleto->codigo_associado)->first();
            $consultaCobranca = Cobranca::where('nosso_numero', $boleto->nosso_numero)->first();
                if($consultaCobranca == null){
                    $cobranca = new Cobranca();
                    $cobranca->descricao_situacao_boleto = $boleto->descricao_situacao_boleto;
                    
                }else{
                    $cobranca = $consultaCobranca;
                    if ($cobranca->descricao_situacao_boleto != $boleto->descricao_situacao_boleto) {
                        switch ($boleto->descricao_situacao_boleto) {
                            case 'BAIXADO':
                                $mensagem = "Oi ".$boleto->nome_associado.", tudo bem? 🤩\nEstamos muito felizes em informá-lo(a) que *seu pagamento foi confirmado!!!*\nMuito obrigado por confiar na gente, e continuar mais um mês conosco!\nTenha um ótimo dia, e *MUITO OBRIGADO!!!* 💙💙";
                                break;
                            case 'ABERTO':
                                break;
                            case 'CANCELADO':
                                break;
                            case 'BAIXADO C\/ PENDÊNCIA':
                                break;
                            case 'EXCLUIDO':
                                break;
                            default:
                                # code...
                                break;
                        }
                    }
                }
                $cobranca->associado_id = intval($boleto->codigo_associado);
                $cobranca->codigo_boleto = $boleto->codigo_boleto;
                $cobranca->nosso_numero = $boleto->nosso_numero;
                $cobranca->valor_boleto = $boleto->valor_boleto;
                $cobranca->data_emissao = date('Y-m-d',strtotime($boleto->data_emissao));
                $cobranca->data_vencimento = date('Y-m-d',strtotime($boleto->data_vencimento));
                $cobranca->data_vencimento_original = date('Y-m-d',strtotime($boleto->data_vencimento_original));
                $cobranca->data_pagamento = date('Y-m-d',strtotime($boleto->data_pagamento));
                $cobranca->data_credito_banco = date('Y-m-d',strtotime($boleto->data_credito_banco));
                $cobranca->descricao_forma_pagamento = $boleto->descricao_forma_pagamento;
                $cobranca->descricao_tipo_cobranca_recorrente = $boleto->descricao_tipo_cobranca_recorrente;
                $cobranca->descricao_tipo_boleto = $boleto->descricao_tipo_boleto;
                $cobranca->save();
                BoletoVeiculo::where('cobrancas_id', $cobranca->id)->delete();
                foreach ($boleto->veiculo as $chave => $veiculo) {
                    $boletoVeiculo = new BoletoVeiculo();
                    $boletoVeiculo->cobrancas_id = $cobranca->id;
                    $boletoVeiculo->veiculo_id = intval($veiculo);
                    $boletoVeiculo->save();
                }
                $this->info($boleto->nome_associado." - R$".$boleto->valor_boleto." - ".$boleto->data_vencimento);
        }
        $this->info("*****DATA 30 OK*****");
        return Command::SUCCESS;
    }
}
