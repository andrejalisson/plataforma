<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Tipoveiculo;

class migrarTipoVeiculo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrar:tipoVeiculoSGA';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $usuarioHinova = Http::withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withToken(env('TOKEN_HINOVA'))->get(env('API_HINOVA').'/listar/tipo-veiculo/ativo');
        $json_str = $usuarioHinova->body();
        $jsonObj = json_decode($json_str);
        foreach ($jsonObj as $tipos) {
            $tipo = new Tipoveiculo();
            $tipo->descricao = $tipos->descricao_tipo;
            $tipo->participacao = $tipos->participacao_minima;
            $tipo->porcentagem = $tipos->porcentagem_fipe;
            $tipo->situacao = $tipos->situacao;
            $tipo->hinova_id = $tipos->codigo_tipo;
            $tipo->save();
            $this->info($tipo->descricao." - Cadastrado");
        }

        return Command::SUCCESS;
    }
}
