<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Associado;
use App\Models\CategoriaVeiculo;
use App\Models\Classificacao;
use App\Models\Combustivel;
use App\Models\Cor;
use App\Models\Modelo;
use App\Models\Montadora;
use App\Models\Tipoveiculo;
use App\Models\Veiculo;
use App\Models\Voluntario;

class migrarVeiculos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrar:veiculos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
            'codigo_situacao' => 1,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 2500,
        ]);
        $json_str = $veiculosHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*2500;
            $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
                'codigo_situacao' => 1,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 2500,
            ]);
            $json_str = $veiculosHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->veiculos as $veiculos) {
                $consultaVeiculo = Veiculo::where('hinova_id', $veiculos->codigo_veiculo)->first();
                if($consultaVeiculo == null){
                    $veiculo = new Veiculo();
                }else{
                    $veiculo = $consultaVeiculo;
                }
                $placa = trim($veiculos->placa);
                $placa = str_replace(" ", "", $placa);
                $placa = str_replace("-", "", $placa);
                $veiculo->placa = $placa;
                $veiculo->chassi = substr($veiculos->chassi, 0, 19);
                $veiculo->renavam = substr($veiculos->renavam, 0, 14);
                $consultaAssociado = Associado::where('hinova_id', $veiculos->codigo_associado)->first();
                if($consultaAssociado != null){
                    $veiculo->codigo_associado = $consultaAssociado->id;
                }else{
                    $veiculo->codigo_associado = null;
                }
                $consultaTipo = Tipoveiculo::where('hinova_id', $veiculos->codigo_tipo)->first();
                if($consultaTipo != null){
                    $veiculo->codigo_tipo = $consultaTipo->id;
                }else{
                    $veiculo->codigo_tipo = null;
                }
                $consultaClassificacao = Classificacao::where('hinova_id', $veiculos->codigo_classificacao)->first();
                if($consultaClassificacao != null){
                    $veiculo->codigo_classificacao = $consultaClassificacao->id;
                }else{
                    $veiculo->codigo_classificacao = null;
                }
                $veiculo->codigo_fipe = $veiculos->codigo_fipe;
                $veiculo->valor_fipe = $veiculos->valor_fipe;
                $consultaMontadora = Montadora::where('hinova_id', $veiculos->codigo_marca)->first();
                if($consultaMontadora != null){
                    $veiculo->codigo_marca = $consultaMontadora->id;
                }else{
                    $veiculo->codigo_marca = null;
                }
                $consultaModelo = Modelo::where('hinova_id', $veiculos->codigo_modelo)->first();
                if($consultaModelo != null){
                    $veiculo->codigo_modelo = $consultaModelo->id;
                }else{
                    $veiculo->codigo_modelo = null;
                }
                $veiculo->ano_fabricacao = $veiculos->ano_fabricacao;
                $veiculo->ano_modelo = $veiculos->ano_modelo;
                $consultaCombustivel = Combustivel::where('hinova_id', $veiculos->codigo_combustivel)->first();
                if($consultaCombustivel != null){
                    $veiculo->codigo_combustivel = $consultaCombustivel->id;
                }else{
                    $veiculo->codigo_combustivel = null;
                }
                $consultaCor = Cor::where('hinova_id', $veiculos->codigo_cor)->first();
                if($consultaCor != null){
                    $veiculo->codigo_cor = $consultaCor->id;
                }else{
                    $veiculo->codigo_cor = null;
                }
                $consultaCategoria = CategoriaVeiculo::where('hinova_id', $veiculos->codigo_categoria)->first();
                if($consultaCategoria != null){
                    $veiculo->codigo_categoria = $consultaCategoria->id;
                }else{
                    $veiculo->codigo_categoria = null;
                }
                $consultaConsultor = Voluntario::where('hinova_id', $veiculos->codigo_voluntario)->first();
                if($consultaConsultor != null){
                    $veiculo->codigo_voluntario = $consultaConsultor->id;
                }else{
                    $veiculo->codigo_voluntario = null;
                }
                $veiculo->data_contrato = date('Y-m-d',strtotime($veiculos->data_contrato));
                $veiculo->situacao = "ATIVO";
                $veiculo->hinova_id = $veiculos->codigo_veiculo;

                $veiculo->save();
                $this->info($veiculo->placa." - Cadastrado");
            }
            sleep(5);
        }


        $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
            'codigo_situacao' => 2,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 2500,
        ]);
        $json_str = $veiculosHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*2500;
            $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
                'codigo_situacao' => 2,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 2500,
            ]);
            $json_str = $veiculosHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->veiculos as $veiculos) {
                $consultaVeiculo = Veiculo::where('hinova_id', $veiculos->codigo_veiculo)->first();
                if($consultaVeiculo == null){
                    $veiculo = new Veiculo();
                }else{
                    $veiculo = $consultaVeiculo;
                }
                $placa = trim($veiculos->placa);
                $placa = str_replace(" ", "", $placa);
                $placa = str_replace("-", "", $placa);
                $veiculo->placa = $placa;
                $veiculo->chassi = substr($veiculos->chassi, 0, 19);
                $veiculo->renavam = substr($veiculos->renavam, 0, 14);
                $consultaAssociado = Associado::where('hinova_id', $veiculos->codigo_associado)->first();
                if($consultaAssociado != null){
                    $veiculo->codigo_associado = $consultaAssociado->id;
                }else{
                    $veiculo->codigo_associado = null;
                }
                $consultaTipo = Tipoveiculo::where('hinova_id', $veiculos->codigo_tipo)->first();
                if($consultaTipo != null){
                    $veiculo->codigo_tipo = $consultaTipo->id;
                }else{
                    $veiculo->codigo_tipo = null;
                }
                $consultaClassificacao = Classificacao::where('hinova_id', $veiculos->codigo_classificacao)->first();
                if($consultaClassificacao != null){
                    $veiculo->codigo_classificacao = $consultaClassificacao->id;
                }else{
                    $veiculo->codigo_classificacao = null;
                }
                $veiculo->codigo_fipe = $veiculos->codigo_fipe;
                $veiculo->valor_fipe = $veiculos->valor_fipe;
                $consultaMontadora = Montadora::where('hinova_id', $veiculos->codigo_marca)->first();
                if($consultaMontadora != null){
                    $veiculo->codigo_marca = $consultaMontadora->id;
                }else{
                    $veiculo->codigo_marca = null;
                }
                $consultaModelo = Modelo::where('hinova_id', $veiculos->codigo_modelo)->first();
                if($consultaModelo != null){
                    $veiculo->codigo_modelo = $consultaModelo->id;
                }else{
                    $veiculo->codigo_modelo = null;
                }
                $veiculo->ano_fabricacao = $veiculos->ano_fabricacao;
                $veiculo->ano_modelo = $veiculos->ano_modelo;
                $consultaCombustivel = Combustivel::where('hinova_id', $veiculos->codigo_combustivel)->first();
                if($consultaCombustivel != null){
                    $veiculo->codigo_combustivel = $consultaCombustivel->id;
                }else{
                    $veiculo->codigo_combustivel = null;
                }
                $consultaCor = Cor::where('hinova_id', $veiculos->codigo_cor)->first();
                if($consultaCor != null){
                    $veiculo->codigo_cor = $consultaCor->id;
                }else{
                    $veiculo->codigo_cor = null;
                }
                $consultaCategoria = CategoriaVeiculo::where('hinova_id', $veiculos->codigo_categoria)->first();
                if($consultaCategoria != null){
                    $veiculo->codigo_categoria = $consultaCategoria->id;
                }else{
                    $veiculo->codigo_categoria = null;
                }
                $consultaConsultor = Voluntario::where('hinova_id', $veiculos->codigo_voluntario)->first();
                if($consultaConsultor != null){
                    $veiculo->codigo_voluntario = $consultaConsultor->id;
                }else{
                    $veiculo->codigo_voluntario = null;
                }
                $veiculo->data_contrato = date('Y-m-d',strtotime($veiculos->data_contrato));
                $veiculo->situacao = "INATIVO";
                $veiculo->hinova_id = $veiculos->codigo_veiculo;

                $veiculo->save();
                $this->info($veiculo->placa." - Cadastrado");
            }
            sleep(5);
        }


        $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
            'codigo_situacao' => 3,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 2500,
        ]);
        $json_str = $veiculosHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*2500;
            $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
                'codigo_situacao' => 3,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 2500,
            ]);
            $json_str = $veiculosHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->veiculos as $veiculos) {
                $consultaVeiculo = Veiculo::where('hinova_id', $veiculos->codigo_veiculo)->first();
                if($consultaVeiculo == null){
                    $veiculo = new Veiculo();
                }else{
                    $veiculo = $consultaVeiculo;
                }
                $placa = trim($veiculos->placa);
                $placa = str_replace(" ", "", $placa);
                $placa = str_replace("-", "", $placa);
                $veiculo->placa = $placa;
                $veiculo->chassi = substr($veiculos->chassi, 0, 19);
                $veiculo->renavam = substr($veiculos->renavam, 0, 14);
                $consultaAssociado = Associado::where('hinova_id', $veiculos->codigo_associado)->first();
                if($consultaAssociado != null){
                    $veiculo->codigo_associado = $consultaAssociado->id;
                }else{
                    $veiculo->codigo_associado = null;
                }
                $consultaTipo = Tipoveiculo::where('hinova_id', $veiculos->codigo_tipo)->first();
                if($consultaTipo != null){
                    $veiculo->codigo_tipo = $consultaTipo->id;
                }else{
                    $veiculo->codigo_tipo = null;
                }
                $consultaClassificacao = Classificacao::where('hinova_id', $veiculos->codigo_classificacao)->first();
                if($consultaClassificacao != null){
                    $veiculo->codigo_classificacao = $consultaClassificacao->id;
                }else{
                    $veiculo->codigo_classificacao = null;
                }
                $veiculo->codigo_fipe = $veiculos->codigo_fipe;
                $veiculo->valor_fipe = $veiculos->valor_fipe;
                $consultaMontadora = Montadora::where('hinova_id', $veiculos->codigo_marca)->first();
                if($consultaMontadora != null){
                    $veiculo->codigo_marca = $consultaMontadora->id;
                }else{
                    $veiculo->codigo_marca = null;
                }
                $consultaModelo = Modelo::where('hinova_id', $veiculos->codigo_modelo)->first();
                if($consultaModelo != null){
                    $veiculo->codigo_modelo = $consultaModelo->id;
                }else{
                    $veiculo->codigo_modelo = null;
                }
                $veiculo->ano_fabricacao = $veiculos->ano_fabricacao;
                $veiculo->ano_modelo = $veiculos->ano_modelo;
                $consultaCombustivel = Combustivel::where('hinova_id', $veiculos->codigo_combustivel)->first();
                if($consultaCombustivel != null){
                    $veiculo->codigo_combustivel = $consultaCombustivel->id;
                }else{
                    $veiculo->codigo_combustivel = null;
                }
                $consultaCor = Cor::where('hinova_id', $veiculos->codigo_cor)->first();
                if($consultaCor != null){
                    $veiculo->codigo_cor = $consultaCor->id;
                }else{
                    $veiculo->codigo_cor = null;
                }
                $consultaCategoria = CategoriaVeiculo::where('hinova_id', $veiculos->codigo_categoria)->first();
                if($consultaCategoria != null){
                    $veiculo->codigo_categoria = $consultaCategoria->id;
                }else{
                    $veiculo->codigo_categoria = null;
                }
                $consultaConsultor = Voluntario::where('hinova_id', $veiculos->codigo_voluntario)->first();
                if($consultaConsultor != null){
                    $veiculo->codigo_voluntario = $consultaConsultor->id;
                }else{
                    $veiculo->codigo_voluntario = null;
                }
                $veiculo->data_contrato = date('Y-m-d',strtotime($veiculos->data_contrato));
                $veiculo->situacao = "PENDENTE";
                $veiculo->hinova_id = $veiculos->codigo_veiculo;

                $veiculo->save();
                $this->info($veiculo->placa." - Cadastrado");
            }
            sleep(5);
        }


        $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
            'codigo_situacao' => 4,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 2500,
        ]);
        $json_str = $veiculosHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*2500;
            $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
                'codigo_situacao' => 4,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 2500,
            ]);
            $json_str = $veiculosHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->veiculos as $veiculos) {
                $consultaVeiculo = Veiculo::where('hinova_id', $veiculos->codigo_veiculo)->first();
                if($consultaVeiculo == null){
                    $veiculo = new Veiculo();
                }else{
                    $veiculo = $consultaVeiculo;
                }
                $placa = trim($veiculos->placa);
                $placa = str_replace(" ", "", $placa);
                $placa = str_replace("-", "", $placa);
                $veiculo->placa = $placa;
                $veiculo->chassi = substr($veiculos->chassi, 0, 19);
                $veiculo->renavam = substr($veiculos->renavam, 0, 14);
                $consultaAssociado = Associado::where('hinova_id', $veiculos->codigo_associado)->first();
                if($consultaAssociado != null){
                    $veiculo->codigo_associado = $consultaAssociado->id;
                }else{
                    $veiculo->codigo_associado = null;
                }
                $consultaTipo = Tipoveiculo::where('hinova_id', $veiculos->codigo_tipo)->first();
                if($consultaTipo != null){
                    $veiculo->codigo_tipo = $consultaTipo->id;
                }else{
                    $veiculo->codigo_tipo = null;
                }
                $consultaClassificacao = Classificacao::where('hinova_id', $veiculos->codigo_classificacao)->first();
                if($consultaClassificacao != null){
                    $veiculo->codigo_classificacao = $consultaClassificacao->id;
                }else{
                    $veiculo->codigo_classificacao = null;
                }
                $veiculo->codigo_fipe = $veiculos->codigo_fipe;
                $veiculo->valor_fipe = $veiculos->valor_fipe;
                $consultaMontadora = Montadora::where('hinova_id', $veiculos->codigo_marca)->first();
                if($consultaMontadora != null){
                    $veiculo->codigo_marca = $consultaMontadora->id;
                }else{
                    $veiculo->codigo_marca = null;
                }
                $consultaModelo = Modelo::where('hinova_id', $veiculos->codigo_modelo)->first();
                if($consultaModelo != null){
                    $veiculo->codigo_modelo = $consultaModelo->id;
                }else{
                    $veiculo->codigo_modelo = null;
                }
                $veiculo->ano_fabricacao = $veiculos->ano_fabricacao;
                $veiculo->ano_modelo = $veiculos->ano_modelo;
                $consultaCombustivel = Combustivel::where('hinova_id', $veiculos->codigo_combustivel)->first();
                if($consultaCombustivel != null){
                    $veiculo->codigo_combustivel = $consultaCombustivel->id;
                }else{
                    $veiculo->codigo_combustivel = null;
                }
                $consultaCor = Cor::where('hinova_id', $veiculos->codigo_cor)->first();
                if($consultaCor != null){
                    $veiculo->codigo_cor = $consultaCor->id;
                }else{
                    $veiculo->codigo_cor = null;
                }
                $consultaCategoria = CategoriaVeiculo::where('hinova_id', $veiculos->codigo_categoria)->first();
                if($consultaCategoria != null){
                    $veiculo->codigo_categoria = $consultaCategoria->id;
                }else{
                    $veiculo->codigo_categoria = null;
                }
                $consultaConsultor = Voluntario::where('hinova_id', $veiculos->codigo_voluntario)->first();
                if($consultaConsultor != null){
                    $veiculo->codigo_voluntario = $consultaConsultor->id;
                }else{
                    $veiculo->codigo_voluntario = null;
                }
                $veiculo->data_contrato = date('Y-m-d',strtotime($veiculos->data_contrato));
                $veiculo->situacao = "INADIMPLENTE";
                $veiculo->hinova_id = $veiculos->codigo_veiculo;

                $veiculo->save();
                $this->info($veiculo->placa." - Cadastrado");
            }
            sleep(5);
        }


        $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
            'codigo_situacao' => 5,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 2500,
        ]);
        $json_str = $veiculosHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*2500;
            $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
                'codigo_situacao' => 5,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 2500,
            ]);
            $json_str = $veiculosHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->veiculos as $veiculos) {
                $consultaVeiculo = Veiculo::where('hinova_id', $veiculos->codigo_veiculo)->first();
                if($consultaVeiculo == null){
                    $veiculo = new Veiculo();
                }else{
                    $veiculo = $consultaVeiculo;
                }
                $placa = trim($veiculos->placa);
                $placa = str_replace(" ", "", $placa);
                $placa = str_replace("-", "", $placa);
                $veiculo->placa = $placa;
                $veiculo->chassi = substr($veiculos->chassi, 0, 19);
                $veiculo->renavam = substr($veiculos->renavam, 0, 14);
                $consultaAssociado = Associado::where('hinova_id', $veiculos->codigo_associado)->first();
                if($consultaAssociado != null){
                    $veiculo->codigo_associado = $consultaAssociado->id;
                }else{
                    $veiculo->codigo_associado = null;
                }
                $consultaTipo = Tipoveiculo::where('hinova_id', $veiculos->codigo_tipo)->first();
                if($consultaTipo != null){
                    $veiculo->codigo_tipo = $consultaTipo->id;
                }else{
                    $veiculo->codigo_tipo = null;
                }
                $consultaClassificacao = Classificacao::where('hinova_id', $veiculos->codigo_classificacao)->first();
                if($consultaClassificacao != null){
                    $veiculo->codigo_classificacao = $consultaClassificacao->id;
                }else{
                    $veiculo->codigo_classificacao = null;
                }
                $veiculo->codigo_fipe = $veiculos->codigo_fipe;
                $veiculo->valor_fipe = $veiculos->valor_fipe;
                $consultaMontadora = Montadora::where('hinova_id', $veiculos->codigo_marca)->first();
                if($consultaMontadora != null){
                    $veiculo->codigo_marca = $consultaMontadora->id;
                }else{
                    $veiculo->codigo_marca = null;
                }
                $consultaModelo = Modelo::where('hinova_id', $veiculos->codigo_modelo)->first();
                if($consultaModelo != null){
                    $veiculo->codigo_modelo = $consultaModelo->id;
                }else{
                    $veiculo->codigo_modelo = null;
                }
                $veiculo->ano_fabricacao = $veiculos->ano_fabricacao;
                $veiculo->ano_modelo = $veiculos->ano_modelo;
                $consultaCombustivel = Combustivel::where('hinova_id', $veiculos->codigo_combustivel)->first();
                if($consultaCombustivel != null){
                    $veiculo->codigo_combustivel = $consultaCombustivel->id;
                }else{
                    $veiculo->codigo_combustivel = null;
                }
                $consultaCor = Cor::where('hinova_id', $veiculos->codigo_cor)->first();
                if($consultaCor != null){
                    $veiculo->codigo_cor = $consultaCor->id;
                }else{
                    $veiculo->codigo_cor = null;
                }
                $consultaCategoria = CategoriaVeiculo::where('hinova_id', $veiculos->codigo_categoria)->first();
                if($consultaCategoria != null){
                    $veiculo->codigo_categoria = $consultaCategoria->id;
                }else{
                    $veiculo->codigo_categoria = null;
                }
                $consultaConsultor = Voluntario::where('hinova_id', $veiculos->codigo_voluntario)->first();
                if($consultaConsultor != null){
                    $veiculo->codigo_voluntario = $consultaConsultor->id;
                }else{
                    $veiculo->codigo_voluntario = null;
                }
                $veiculo->data_contrato = date('Y-m-d',strtotime($veiculos->data_contrato));
                $veiculo->situacao = "NEGADO";
                $veiculo->hinova_id = $veiculos->codigo_veiculo;

                $veiculo->save();
                $this->info($veiculo->placa." - Cadastrado");
            }
            sleep(5);
        }


        $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
            'codigo_situacao' => 6,
            'inicio_paginacao' => 0,
            'quantidade_por_pagina' => 2500,
        ]);
        $json_str = $veiculosHinova->body();
        $jsonObj = json_decode($json_str);
        sleep(5);
        $x = 0;
        for ($i=1; $i <= $jsonObj->numero_paginas; $i++){
            $y = $x*2500;
            $veiculosHinova = Http::timeout(-1)->withHeaders(['Accept' => 'application/json','Content-Type' => 'application/json'])->withOptions(["verify"=>false])->withToken(env('TOKEN_HINOVA'))->post(env('API_HINOVA').'/listar/veiculo',[
                'codigo_situacao' => 6,
                'inicio_paginacao' => $y,
                'quantidade_por_pagina' => 2500,
            ]);
            $json_str = $veiculosHinova->body();
            $jsonObj = json_decode($json_str);
            $x++;
            foreach ($jsonObj->veiculos as $veiculos) {
                $consultaVeiculo = Veiculo::where('hinova_id', $veiculos->codigo_veiculo)->first();
                if($consultaVeiculo == null){
                    $veiculo = new Veiculo();
                }else{
                    $veiculo = $consultaVeiculo;
                }
                $placa = trim($veiculos->placa);
                $placa = str_replace(" ", "", $placa);
                $placa = str_replace("-", "", $placa);
                $veiculo->placa = $placa;
                $veiculo->chassi = substr($veiculos->chassi, 0, 19);
                $veiculo->renavam = substr($veiculos->renavam, 0, 14);
                $consultaAssociado = Associado::where('hinova_id', $veiculos->codigo_associado)->first();
                if($consultaAssociado != null){
                    $veiculo->codigo_associado = $consultaAssociado->id;
                }else{
                    $veiculo->codigo_associado = null;
                }
                $consultaTipo = Tipoveiculo::where('hinova_id', $veiculos->codigo_tipo)->first();
                if($consultaTipo != null){
                    $veiculo->codigo_tipo = $consultaTipo->id;
                }else{
                    $veiculo->codigo_tipo = null;
                }
                $consultaClassificacao = Classificacao::where('hinova_id', $veiculos->codigo_classificacao)->first();
                if($consultaClassificacao != null){
                    $veiculo->codigo_classificacao = $consultaClassificacao->id;
                }else{
                    $veiculo->codigo_classificacao = null;
                }
                $veiculo->codigo_fipe = $veiculos->codigo_fipe;
                $veiculo->valor_fipe = $veiculos->valor_fipe;
                $consultaMontadora = Montadora::where('hinova_id', $veiculos->codigo_marca)->first();
                if($consultaMontadora != null){
                    $veiculo->codigo_marca = $consultaMontadora->id;
                }else{
                    $veiculo->codigo_marca = null;
                }
                $consultaModelo = Modelo::where('hinova_id', $veiculos->codigo_modelo)->first();
                if($consultaModelo != null){
                    $veiculo->codigo_modelo = $consultaModelo->id;
                }else{
                    $veiculo->codigo_modelo = null;
                }
                $veiculo->ano_fabricacao = $veiculos->ano_fabricacao;
                $veiculo->ano_modelo = $veiculos->ano_modelo;
                $consultaCombustivel = Combustivel::where('hinova_id', $veiculos->codigo_combustivel)->first();
                if($consultaCombustivel != null){
                    $veiculo->codigo_combustivel = $consultaCombustivel->id;
                }else{
                    $veiculo->codigo_combustivel = null;
                }
                $consultaCor = Cor::where('hinova_id', $veiculos->codigo_cor)->first();
                if($consultaCor != null){
                    $veiculo->codigo_cor = $consultaCor->id;
                }else{
                    $veiculo->codigo_cor = null;
                }
                $consultaCategoria = CategoriaVeiculo::where('hinova_id', $veiculos->codigo_categoria)->first();
                if($consultaCategoria != null){
                    $veiculo->codigo_categoria = $consultaCategoria->id;
                }else{
                    $veiculo->codigo_categoria = null;
                }
                $consultaConsultor = Voluntario::where('hinova_id', $veiculos->codigo_voluntario)->first();
                if($consultaConsultor != null){
                    $veiculo->codigo_voluntario = $consultaConsultor->id;
                }else{
                    $veiculo->codigo_voluntario = null;
                }
                $veiculo->data_contrato = date('Y-m-d',strtotime($veiculos->data_contrato));
                $veiculo->situacao = "INATIVO/PAGAMENTO";
                $veiculo->hinova_id = $veiculos->codigo_veiculo;

                $veiculo->save();
                $this->info($veiculo->placa." - Cadastrado");
            }
            sleep(5);
        }
        return Command::SUCCESS;
    }
}
